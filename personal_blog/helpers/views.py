from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View, TemplateView, DetailView


class ParamMixin():
    form_class = None
    template_name = None
    model = None
    redirect_url = ''
    context_key = 'objects'
    key_kwarg = 'pk'

    def get_redirect_url(
            self):  # просто геттер для url - ну например с помощью этого мы можем брать 'home_page', 'post_create'-> вот такие url имена мы можем брать
        return self.redirect_url

    def get_object(self):
        pk = self.kwargs[self.key_kwarg]
        return get_object_or_404(self.model, pk=pk)


class CustomFormView(View, ParamMixin):  # мы импортируем здесь generic view и с этого и мы построили свой FormView
    form_class = None  # передаем туда класс формы например у нас есть формы: PostForm, CommentForm, AuthorForm вот этих то мы и передаем здесь
    template_name = None  # мы будем сюда передовать путь шаблона -> html страница
    redirect_url = ''  # ссылка для перенапровление

    def get(self, request, *args, **kwargs):  # метод для обработки get запросов
        form = self.form_class()  # наша форма -> PostForm, AuthorForm ...
        context = self.get_context_data(
            form=form)  # контекст функция getter должны передовать форму чтоб мы в шоблоне могли работать
        return render(request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):  # для работы с post запросами
        form = self.form_class(data=request.POST)
        if form.is_valid():
            return self.form_valid(
                form)  # это метод который мы сами будем создать для того чтоб если форма валидна выполнялась эта функция
        else:
            return self.form_invalid(
                form)  # это точно также если у нас форма не валидна то есть есть ошибки в форме то мы будем вызвать этот метод инвалид

    def form_valid(self, form):
        return redirect(self.get_redirect_url())  # перенаправляем на url адрес если наша форма валидна

    def form_invalid(self,
                     form):  # форма не прошел валидацию в этом случае мы должны render to smt like this { return render(request, 'posts/create.html', context = context) }
        context = self.get_context_data(form=form)
        return render(self.request, self.template_name, context=context)

    def get_context_data(self, **kwargs):  # **kwargs это именованный аргумент, словарь с аргументами key and value
        return kwargs


class CustomListView(TemplateView, ParamMixin):
    model = None  # даем имя модели
    context_key = 'objects'  # имя шаблона

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context[self.context_key] = self.model.objects.all()
        return context

    def get_objects(self):
        return self.model.objects.all()


class CustomDetailView(TemplateView, ParamMixin):
    context_key = 'object'
    model = None
    key_kwarg = 'pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context[self.context_key] = self.get_object()
        return context


class CustomCreateView(View, ParamMixin):
    form_class = None
    template_name = ''
    model = None
    redirect_url = None

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save()
        return redirect(self.get_redirect_url())

    def form_invalid(self, form):
        context = {'form': form}
        return render(self.request, self.template_name, context)


class CustomUpdateView(View, ParamMixin):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(instance=self.object)
        context = self.get_context_data(form=form)
        return render(request, self.template_name, context=context)

    def get_context_data(self, **kwargs):
        context = self.kwargs.copy()
        context[self.context_key] = self.object
        context.update(kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save()
        return redirect(self.get_redirect_url())

    def form_invalid(self, form):
        context = self.get_context_data(form=form)
        return render(self.request, self.template_name, {'context': context})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(instance=self.object, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

class CustomDeleteView(View, ParamMixin):
    confirm_deletion = True

    def get(self, request, **kwargs):
        self.object = self.get_object()
        if self.confirm_deletion:
            return render(request, self.template_name, self.get_context_data())
        self.perform_deletion()
        return redirect(self.get_redirect_url())

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        