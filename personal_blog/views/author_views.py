from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from personal_blog.forms import AuthorForm
from personal_blog.models import Author
from django.views.generic import View, ListView, DeleteView, CreateView, UpdateView
from django.contrib.auth.models import User


def author_create_view(request):
    if request.method == 'GET':
        form = AuthorForm()
        return render(request, 'authors/create.html', context={'form': form})
    elif request.method == 'POST':
        form = AuthorForm(request.POST)

        if form.is_valid():
            author = Author.objects.create(name=request.POST.get('name'))
            return redirect('author_list')
        else:
            return render(request, 'authors/create.html', context={'form': form})


'''
    def author_list_view(request):
        return render(request, 'authors/list.html', context={'authors': Author.objects.all()})
'''


class AuthorListView(ListView):
    template_name = 'authors/list.html'
    model = Author
    context_object_name = 'authors'
    ordering = ['id']
    paginate_by = 10
    paginate_orphans = 0

    def get_queryset(self):
        users = User.objects.all()
        for user in users:
            if not Author.objects.filter(name=user).exists():
                Author.objects.create(name=user)

        sort_by = self.request.GET.get('sort_by')
        queryset = Author.objects.all()
        if sort_by:
            queryset = queryset.order_by(sort_by)

        return queryset

# class AuthorView(View):
#     def dispatch(self, request, *args, **kwargs):
#         if request.method == 'GET':
#             return self.get(request, *args, **kwargs)
#         elif request.method == 'POST':
#             return self.post(request, *args, **kwargs)
#
#     def get(self, request, *args, **kwargs):
#         form = AuthorForm()
#         return render(request, 'authors/create.html', context={'form': form})
#
#     def post(self, request, *args, **kwargs):
#         form = AuthorForm(request.POST)
#
#         if form.is_valid():
#             author = Author.objects.create(name=request.POST.get('name'))
#             return redirect('author_list')
#         else:
#             return render(request, 'authors/create.html', context={'form': form})

class AuthorCreateView(CreateView):
    template_name = 'authors/create.html'
    model = Author
    form_class = AuthorForm
    success_url = reverse_lazy('author_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['authors'] = Author.objects.all()
        return context


class AuthorDeleteView(DeleteView):
    model = Author
    success_url = reverse_lazy('author_list')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class AuthorUpdateView(UpdateView):
    model = Author
    template_name = 'authors/update.html'
    form_class = AuthorForm
    context_object_name = 'author'
    success_url = reverse_lazy('author_list')
