from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.utils.http import urlencode
from django.views.generic import View, TemplateView, FormView, ListView, DetailView, DeleteView
from personal_blog.forms import CommentForm, SearchForm, PostForm
from personal_blog.helpers.views import CustomListView, CustomFormView, CustomDetailView, CustomCreateView, \
    CustomUpdateView
from personal_blog.models import Post, Author, Comment

''' Just by using View class'''
# class PostDetailView(View):
#     def get(self, request, *args, **kwargs):
#         form = CommentForm()
#         post = get_object_or_404(Post, pk=kwargs.get('pk'))
#         return render(request, 'posts/view.html', context={'post': post, 'form': form})

''' By using templateView'''


# class PostDetailView(TemplateView):
#     template_name = 'posts/view.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['post']  = get_object_or_404(Post, pk=self.kwargs.get('pk'))
#         return context

# class PostDetailView(CustomDetailView):
#     template_name = 'posts/view.html'
#     context_key = 'post'
#     model = Post
#

# class PostCreateView(CustomFormView):
#     form_class = PostForm
#     template_name = 'posts/create.html'
#
#     def form_valid(self, form):
#         """
#         data = {}
#         tags = form.cleaned_data.pop('tags')
#
#         for key, value in form.cleaned_data.items():  # полагаю что здесь все наши данные к примеру { title : Crime and Punishment, author: Dostoevsky, body: such a great book}
#             if value is not None:  # key = name, value = Crime and Punishment, key = author , value = Dostoevsky
#                 data[key] = value
#         self.post = Post.objects.create(**data)  # и наша пост будет записать все эти данные в себе
#         self.post.tags.set(tags)
#         """
#
#         self.post = form.save()
#         return super().form_valid(
#             form)  # мы должны переопределять redirect_url потому что джанго не знает куда отправлять данные
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['authors'] = Author.objects.all()
#         return context
#
#     def get_redirect_url(self):
#         return reverse('post_list')

# class PostUpdateView(FormView):
#     template_name = 'posts/update.html'
#     form_class = PostForm
#
#     def dispatch(self, request, *args, **kwargs):
#         self.my_post = self.get_object()
#         return super().dispatch(request, *args, **kwargs)
#
#     def get_object(self):
#         pk = self.kwargs.get('pk')
#         return get_object_or_404(Post, pk=pk)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['post'] = self.my_post
#         return context
#
#     # def get_initial(self):
#     #     initial = {} # для начала пустой словарик
#     #
#     #     for key in ['title', 'body', 'author', 'tags']:
#     #         initial[key] = getattr(self.post, key)
#     #     initial['tags'] = self.post.tags.all()
#     #     return initial
#
#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs['instance'] = self.my_post
#         return kwargs
#
#     def form_valid(self, form):
#         # tags = form.cleaned_data.pop('tags')
#         # for key, value in form.cleaned_data.items():
#         #     if value:
#         #         setattr(self.post, key, value)
#         # self.post.save()
#         # self.post.tags.set(tags)
#         self.my_post = form.save()
#         return super().form_valid(form)
#
#     def get_success_url(self):
#         return reverse('post_detail', kwargs={'pk': self.my_post.pk})


class PostDetailView(DetailView):
    template_name = 'posts/view.html'
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_form = CommentForm()
        my_post = self.object
        comments = my_post.Comments
        context['comments'] = comments
        context['comment_form'] = comment_form
        return context


class PostListView(ListView):
    template_name = 'posts/list.html'
    model = Post
    context_object_name = 'posts'
    form = SearchForm
    search_value = None

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_search_form(self):
        return self.form(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get("search")
        return None

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(title__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset


class PostCreateView(CustomCreateView):
    template_name = 'posts/create.html'
    model = Post
    form_class = PostForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        post.save()
        print(post.author)
        return super().form_valid(form)

    def get_redirect_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.pk})


class PostUpdateView(CustomUpdateView):
    model = Post
    template_name = 'posts/update.html'
    form_class = PostForm
    context_key = 'post'
    permission_required = 'personal_blog.change_post'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect('home_page')
        # if not user.has_perm('personal_blog.change_post'):
        #     raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)

    def get_redirect_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.pk})


class PostDeleteView(DeleteView):
    template_name = 'posts/delete.html'
    model = Post

    def get_success_url(self):
        return reverse('post_list')
