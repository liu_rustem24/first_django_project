from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, DeleteView

from personal_blog.forms import CommentForm
from personal_blog.helpers.views import CustomCreateView, CustomUpdateView
from personal_blog.models import Comment, Post

"""
class CommentView(View):
    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)
        post_pk = kwargs.get('post_pk')
        if form.is_valid():
            post = get_object_or_404(Post, pk=post_pk)
            post.Comments.create(
                text=request.POST.get("text"),
                author=request.POST.get("author")
            )
            return redirect('post_detail', post_pk)
"""


class CommentCreateView(CreateView):
    model = Comment
    template_name = 'comments/create.html'
    form_class = CommentForm

    def form_valid(self, form):
        my_post = get_object_or_404(Post, pk=self.kwargs['post_pk'])
        comment = form.save(commit=False)
        comment.post = my_post
        comment.author = self.request.user
        comment.save()
        return redirect('post_detail', pk=my_post.pk)


class CommentUpdateView(CustomUpdateView):
    model = Comment
    template_name = 'comments/update.html'
    context_key = 'comment'
    form_class = CommentForm
    key_kwarg = 'post_pk'

    def get_redirect_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.pk})


class CommentDeleteView(DeleteView):
    model = Comment
    form_class = CommentForm

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.post.pk})
