from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView
from personal_blog.models import Tag
from personal_blog.forms import TagForm


class TagCreateView(CreateView):
    template_name = 'posts/create.html'
    form_class = TagForm
    model = Tag
    context_object_name = 'tag'
    success_url = reverse_lazy('post_create')

