from django.urls import path

from personal_blog.views.comments_view import CommentCreateView, CommentUpdateView, CommentDeleteView
from personal_blog.views.post_views_classView import PostDetailView, PostCreateView, PostUpdateView, PostListView, PostDeleteView
from personal_blog.views.post_views_functions import post_delete_view
from personal_blog.views.tag_views import TagCreateView

urlpatterns = [
    path('detail/<int:pk>', PostDetailView.as_view(), name="post_detail"),
    path('create/', PostCreateView.as_view(), name="post_create"),
    path('list/', PostListView.as_view(), name='post_list'),
    path('update/<int:pk>', PostUpdateView.as_view(), name="post_update"),
    path('delete/<int:pk>', PostDeleteView.as_view(), name="post_delete"),
    path('detail/<int:post_pk>/comment/create', CommentCreateView.as_view(), name="comment_create"),
    path('detail/<int:post_pk>/comment/update', CommentUpdateView.as_view(), name="comment_update"),
    path('detail/<int:post_pk>/comment/<int:pk>/delete/', CommentDeleteView.as_view(), name='comment_delete'),
    path('tag/create/', TagCreateView.as_view(), name="tag_create")
]