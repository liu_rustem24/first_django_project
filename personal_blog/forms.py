from django import forms
from django.core.exceptions import ValidationError
from django.forms import widgets

from personal_blog.helpers.validators import at_least_3
from personal_blog.models import Tag, Author, Post, Comment


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ['author']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Title'}),
            'body': forms.Textarea(attrs={'class': 'form-control mb-3', 'rows': '5', 'placeholder': 'Body'}),
            'author': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Author'}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

    def clean(self):
        cleaned_data = super().clean()
        body = cleaned_data.get('body')
        title = cleaned_data.get('title')
        if body and title and body == title:
            raise ValidationError("Post title and body cannot be the same")
        return cleaned_data

    def clean_title(self):
        title = self.cleaned_data['title']
        if len(title) < 5:
            raise ValidationError("Title is too short!")
        return title

    def clean_body(self):
        body = self.cleaned_data['body']
        if body == "new":
            raise ValidationError("Title and body should not be the same")
        return body


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Author'})}


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control mb-3', 'placeholder': 'Text'}),
            # 'author': forms.TextInput(attrs={'class': 'form-control'})
        }


class SearchForm(forms.Form):
    search = forms.CharField(max_length=200, required=False, label='Search')


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Tag name'})}
