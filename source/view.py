from django.shortcuts import render
from django.views.generic import View, TemplateView
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

class IndexView(TemplateView):
    template_name = 'index.html'

# @login_required
def index(request):
    return render(request, 'index.html')
# class IndexView(View):
#     def get(self, request, *args, **kwargs):
#         return render(request, 'index.html')

