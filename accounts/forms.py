from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth import get_user_model
from accounts.models import Profile


# class MyUserCreationForm(forms.ModelForm):
#     password = forms.CharField(
#         label='Password',
#         required=True,
#         strip=False,
#         widget=forms.PasswordInput()
#     )
#     password_confirm = forms.CharField(
#         label='Password',
#         required=True,
#         strip=False,
#         widget=forms.PasswordInput()
#     )
#
#     def clean(self):
#         cleaned_data = super().clean()
#         password = cleaned_data.get("password")
#         password_confirm = cleaned_data.get("password_confirm")
#
#         if password and password_confirm and password != password_confirm:
#             raise forms.ValidationError("Passwords are not same!")
#
#     def save(self, commit=True):
#         user = super().save(commit=False)
#         user.set_password(self.cleaned_data["password"])
#         if commit:
#             user.save()
#         return user
#
#     class Meta:
#         model = User
#         fields = ['username', 'first_name', 'last_name', 'email', 'password', 'password_confirm']

class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = User

        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]

        field_classes = {'username': UsernameField}


class UserEditForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email']
        labels = {'first_name': 'First Name', 'last_name': 'Last Name', 'email': 'Email'}

        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'email Name'})
        }


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user']

        widgets = {
            'birth_date': forms.DateInput(attrs={'class': 'form-control'}),
            'avatar': forms.FileInput(attrs={'class': 'form-control'})
        }


class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label='New Password', strip=False)
    password_confirm = forms.CharField(widget=forms.PasswordInput(), label='Confirm Password', strip=False)
    old_password = forms.CharField(widget=forms.PasswordInput(), label='Old Password', strip=False)

    def clean_password_confirm(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError("passwords don't match")
        return password_confirm

    def clean_old_password(self):
        old_password = self.cleaned_data.get("old_password")

        if not self.instance.check_password(old_password):
            raise forms.ValidationError("Old password is incorrect")
        return old_password

    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ['old_password', 'password', 'password_confirm']