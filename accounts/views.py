from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse
from django.views.generic import DetailView
from accounts.forms import MyUserCreationForm, UserEditForm, ProfileEditForm, PasswordChangeForm
from personal_blog.models import Author
from django.contrib.auth import get_user_model
from accounts.models import Profile
from django.views.generic import UpdateView


def login_view(request):
    context = {}
    print(request.GET)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)  # проверяет есть ли такой пользователь
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('home_page')

        else:
            context['has_error'] = True
    return render(request=request, template_name='accounts/login.html', context=context)


def logout_view(request):
    logout(request)
    return redirect('home_page')


def register_view(request):
    if request.method == 'POST':
        form = MyUserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            author = Author.objects.create(name=user)
            Profile.objects.create(user=user)
            login(request, user)
            return redirect('home_page')
    else:
        form = MyUserCreationForm()
    return render(request, 'accounts/registration.html', context={'form': form})


class UserDetailView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'accounts/profile.html'
    context_object_name = 'user_object'
    paginate_related_by = 5
    paginate_related_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        posts = self.object.posts.order_by('-created_at')
        paginator = Paginator(posts, self.paginate_related_by, orphans=self.paginate_related_orphans)
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        context['page_object'] = page
        context['posts'] = page.object_list
        context['is_paginated'] = page.has_other_pages()
        return context


# @login_required
class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = UserEditForm
    template_name = 'accounts/user_edit.html'
    context_object_name = 'user_object'

    def get_context_data(self, **kwargs):
        # context = super().get_context_data()
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
        return super().get_context_data(**kwargs)

    def get_profile_form(self):
        profile_form = {'instance': self.object.profile}
        if self.request.method == 'POST':
            profile_form['data'] = self.request.POST
            profile_form['files'] = self.request.FILES
        return ProfileEditForm(**profile_form)

    def get_success_url(self):
        return reverse('user_detail', kwargs={'pk': self.object.pk})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()

        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return render(context)

    def get_object(self, queryset=None):
        return self.request.user


# @login_required
class UserPasswordChangeView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/password_change.html'
    form_class = PasswordChangeForm
    context_object_name = 'user_object'

    def form_valid(self, form):
        user = form.save()
        update_session_auth_hash(self.request, user)
        return redirect(self.get_success_url())

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('login')
