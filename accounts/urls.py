from django.urls import path
from accounts.views import login_view, logout_view, register_view, UserDetailView, UserUpdateView, UserPasswordChangeView
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    path('login/', login_view, name='login'),
    # path('/logout', logout_view, name='logout')
    # path('login/', LoginView.as_view(), name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register_view, name='register'),
    path('<int:pk>/', UserDetailView.as_view(), name='user_detail'),
    path('edit/', UserUpdateView.as_view(), name='user_edit'),
    path('change_password/', UserPasswordChangeView.as_view(), name='password_change')
]